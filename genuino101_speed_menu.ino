#include "Menu.h"
#include <SPI.h>
#include <Wire.h>
#include <CurieIMU.h>
#include <SoftwareSerial.h>

#define MAX_BT_COMMAND_LENGTH 40   // максимальный размер команды по bluetooth
#define PASSWORDS_COUNT       4    // количество хранимых (на текущий момент) паролей 
#define KEY_PIN               4    // цифровой порт для входа в режим программирования HC-05
#define MESSAGING_DELAY       500

#define NEXT_BTN    7
#define ENTER_BTN   8
#define OLED_MOSI   9              // порты для OLED
#define OLED_CLK   10
#define OLED_DC    11
#define OLED_CS    12
#define OLED_RESET 13

Adafruit_SSD1306 display(OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);
Menu main_menu;

SoftwareSerial BTSerial(2, 3);

const float epsRotate = 0.09;
const float eps = 0.09;
const float slowerSecond = 4;

const int pairing_timeout = 5;                // таймаут для сопряжения в секундах
char command_buffer[MAX_BT_COMMAND_LENGTH];   // массив для получения команды по bluetooth
String passwords[PASSWORDS_COUNT] = {"0597", "1234", "0000", "0305"};
String mac_devices[MAX_AVALIABLE_DEVICES];
String names_of_devices[MAX_AVALIABLE_DEVICES];
boolean is_connected = false;
boolean is_car = false;

void InitializeIMU()
{
  CurieIMU.begin();
  CurieIMU.setAccelerometerRange(2);
}

void InitializeCOM()
{
  BTSerial.begin(38400);
}

void InitializeDevice()
{
  BTSerial.flush();
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);

  BTSerial.print("AT+RESET\r\n");
  delay(1000);
  String answer = BT_Answer("AT");
  display.setCursor(0, 0);
  display.println("AT");
  display.display();

  delay(MESSAGING_DELAY);

  display.setCursor(0, 10);
  display.println(answer);
  display.display();

  delay(MESSAGING_DELAY);

  display.setCursor(0, 20);
  display.println("AT+INIT");
  display.display();

  delay(MESSAGING_DELAY);

  BTSerial.flush();
  answer = BT_Answer("AT+INIT");

  display.setCursor(0, 30);
  display.println(answer);
  display.display();

  delay(MESSAGING_DELAY);
}

void TankControl()        // дистанционное управление на акселерометре
{
  float ax, ay, az;
  CurieIMU.readAccelerometerScaled(ax, ay, az);

  String left;
  String right;
  createCommand(ax, ay, az, left, right);

  if (!digitalRead(NEXT_BTN))     // если зеленая кнопка нажата
  {
    BTSerial.println(left + "\r\n");
    BTSerial.println(right + "\r\n");   
  }
  else 
  {
    BTSerial.println("HALT\r\n");
  }
}

void CarControl()
{
  float ax, ay, az;
  CurieIMU.readAccelerometerScaled(ax, ay, az);

  String left;
  String right;
  carMove(ax, ay, az, left, right);
  if (!digitalRead(NEXT_BTN))     // если зеленая кнопка нажата
  {
    BTSerial.println(left + "\r\n");
    BTSerial.println(right + "\r\n");   
  }
  else 
  {
    BTSerial.println("HALT\r\n");
  }
}

void createCommand(float x, float y, float z, String& left, String& right)  // создание команд для посылки
{
  float newX = x * x;
  float newZ = z * z;
  right = "R";
  left = "L";

  float throttle = 50;
  float addSpeed = min(newZ * 100, 50);
  float secondThrottle = 50;

  if (z >= 0)
  {
    throttle += addSpeed;
    secondThrottle += (1 - newX) * addSpeed / slowerSecond;
  }
  else
  {
    throttle -= addSpeed;
    secondThrottle -= (1 - newX) * addSpeed / slowerSecond;
  }

  if (abs(x) > eps)
  {
    //танковый поворот
    if (abs(z) <= epsRotate)
    {
      addSpeed = min(newX * 100, 50);
      if (x > 0)
      {
        left += (int) (50 + addSpeed);
        right += (int) (50 - addSpeed);
      }
      else
      {
        right += (int)(50 + addSpeed);
        left += (int) (50 - addSpeed);
      }
    }
    else
    {
      //поворот
      if (x > 0)
      {
        left += (int)throttle;
        right += (int)secondThrottle;
      }
      else
      {
        right += (int)throttle;
        left += (int)secondThrottle;
      }
    }
  }
  else
  {
    left += (int)throttle;
    right += (int)throttle;
  }
}

void carMove(float x, float y, float z, String& Speed, String& rotate)
{
  Speed = "L";
  rotate = "S";
  float newX = x * x;
  float newZ = z * z;

  float throttle = 50;
  float addSpeed = min(newZ * 100, 50);
  if (z >= 0)
    throttle += addSpeed;
  else
    throttle -= addSpeed;
  Speed += (int)throttle;
  int rot = 50 + ((x > 0 ) ? 1 : -1 ) * min(newX * 100, 50);
  rotate += (int)rot;
}

void ClearArray(String *container, int size)
{
  for (int i = 0; i < size; ++i)
    container[i] = "";
}

String BT_Answer(String to_send)  //получение ответа от BT-модуля
{
  to_send += "\r\n";
  BTSerial.print(to_send);
  String command = GetCommand();
  BTSerial.flush();
  return command;
}

int GetDevices(String *devices)     // заполнение массива MAC-адресами доступных устройств
{ // возвращает количество найденных устройств
  BTSerial.println("AT+INQ\r\n");
  String device = "";
  int i = 0;
  for (i = 0; i < MAX_AVALIABLE_DEVICES; ++i)
  {
    device = GetCommand();
    if (device == "OK" || device == "" || device == " ")         // когда устройства закончились
      return i;
      
    //отрезаем служебную команду +INQ: в начале:
    device = device.substring(5, device.length());
    int end_index = 0;
    //ищем индекс, где заканчивается реальный MAC-адрес:
    for (int j = 0; j < device.length(); j++)
    {
      char current = device[j];
      if (current == ',')
      {
        end_index = j;
        break;
      }
    }

    // отрезаем мусор в конце адреса:
    device = device.substring(0, end_index);
    // приводим адрес к готовому для дальнейших запросов виду:
    device.replace(':', ',');
    devices[i] = device;
  }
}

// заполнение массива names именами устройств по адресам из macs:
void MacToName(String* macs, String* names, int size)
{
  for (int i = 0; i < size; ++i)
  {
    String current_mac = macs[i];
    if (current_mac == "")
    {
      names[i] = "";
      continue;
    }
    BTSerial.flush();
    if (current_mac == "98D3,31,806E07")
      names[i] = "MARS16";
    else if (current_mac == "2015,7,206401")
      names[i] = "CAR-MEXMAT";    
  }
}

String Pair(String variant, String* passwords)
{
  String answer = "";
  for (int i = 0; i < PASSWORDS_COUNT; ++i)
  {
    BTSerial.print("AT+PSWD=" + passwords[i] + "\r\n");
    delay(500);
    BTSerial.flush();
    answer = BT_Answer("AT+PAIR=" + variant + "," + (String)pairing_timeout);
    delay(500);
    display.clearDisplay();
    display.setCursor(0, 0);
    display.println(String(i) + ": " + answer);
    display.display();
    if (answer == "OK")
      break;
    if (i == PASSWORDS_COUNT - 1)
      return "PSWD_FAIL";
  }
  return "OK";
}

String Link(String variant)
{
  String answer = BT_Answer("AT+LINK=" + variant);
  if (answer != "OK")
    return "CODE_LINK_FAILURE";
  return "OK";
}

String GetCommand() // получение команды из COM-порта, если команды нет, то возвращается пустая строка
{
  unsigned long time_after = 0;
  unsigned long time_before = millis();
  while (BTSerial.available() == 0)   // ждем прихода данных
  {
    time_after = millis();
    if (time_after - time_before >= pairing_timeout * 1000)  // таймаут ожидания ответа
      return "";
  }

  int incoming_byte = BTSerial.readBytesUntil('\n', command_buffer, MAX_BT_COMMAND_LENGTH);   //грузим данные в буфер
  String command = command_buffer;
  command = command.substring(0, incoming_byte);
  command.replace("\r", "");
  command.replace("\n", "");
  return command;
}

void atLink()
{
  String name_variant = names_of_devices[main_menu.current_position];
  if (name_variant == "CAR-MEXMAT")
    is_car = true;
  String variant = mac_devices[main_menu.current_position];
  String answer = Pair(variant, passwords);

  display.clearDisplay();
  display.setCursor(0, 0);
  display.println("Pairing successful");
  display.display();
  delay(1000);

  answer = Link(variant);
  display.clearDisplay();
  display.setCursor(0, 0);
  display.println("Link successful");
  display.display();
  delay(1000);

  digitalWrite(KEY_PIN, LOW);
  delay(50);

  is_connected = true;
}

void atNames()
{
  display.clearDisplay();
  ClearArray(mac_devices, MAX_AVALIABLE_DEVICES);
  ClearArray(names_of_devices, MAX_AVALIABLE_DEVICES);

  int device_count = GetDevices(mac_devices);
  if (device_count == 0)
  {
    main_menu[0].title = "NO DEVICES. EXIT?";
    main_menu[0].action = &atMain;
    main_menu[0].cursor_shift = 0;
  }
  else
  {
    display.clearDisplay();
    MacToName(mac_devices, names_of_devices, device_count);

    main_menu.size = device_count;

    int i;
    for (i = 0; i < device_count; ++i)
    {
      main_menu[i].title = names_of_devices[i];
      main_menu[i].action = &atLink;
      main_menu[i].cursor_shift = 0;
    }
    i++;
    main_menu[i].title = "Back to menu ->";
    main_menu[i].action = &atMain;
    main_menu[i].cursor_shift = 0;

    main_menu.current_position = 0;
    main_menu.print(display);
  }

}

void atMain()
{
  display.clearDisplay();
  main_menu.size = 3;
  main_menu[0].title = "1. Names";
  main_menu[0].action = &atNames;
  main_menu[0].cursor_shift = 0;

  main_menu[1].title = "2. Addresses";
  main_menu[1].action = NULL;
  main_menu[1].cursor_shift = 0;

  main_menu[2].title = "3. About";
  main_menu[2].action = &atAbout;
  main_menu[2].cursor_shift = 0;
  main_menu.current_position = 0;
  main_menu.print(display);
}

void atAbout()
{
  display.clearDisplay();
  main_menu.size = 5;
  main_menu[0].title = "Anton Fedyashov";
  main_menu[0].action = NULL;
  main_menu[0].cursor_shift = 0;

  main_menu[1].title = "Ilya Yakubovskiy";
  main_menu[1].action = NULL;
  main_menu[1].cursor_shift = 0;

  main_menu[2].title = "3.2, 3.8 in MMCS";
  main_menu[2].action = NULL;
  main_menu[2].cursor_shift = 0;

  main_menu[3].title = "SFEDU Robolab";
  main_menu[3].action = NULL;
  main_menu[3].cursor_shift = 0;

  main_menu[4].title = "Back to menu ->";
  main_menu[4].action = &atMain;
  main_menu[4].cursor_shift = 0;
  main_menu.current_position = 0;
  main_menu.print(display);
}

void WaitForClick()
{
  while (true)
  {
    boolean enter = digitalRead(ENTER_BTN);
    boolean next = digitalRead(NEXT_BTN);
    if (!next)
      main_menu.goForward(display);
    if (!enter)
      break;
    delay(100);
  }
}

void setup()
{
  InitializeCOM();
  InitializeIMU();

  pinMode(KEY_PIN, OUTPUT);          // перевод HC-05 в режим программирования
  digitalWrite(KEY_PIN, HIGH);
  pinMode(ENTER_BTN, INPUT_PULLUP);  // подготовка кнопок
  pinMode(NEXT_BTN, INPUT_PULLUP);

  display.begin(SSD1306_SWITCHCAPVCC);  // инициализация дисплея и показ штатной картинки
  display.display();
  delay(2000);

  InitializeDevice();

  atMain();           //Отрисовка главного меню
  while (true)        //Обработка событий
  {
    if (is_connected)
    {
      if (is_car)
      {
        CarControl();
        delay(100);
      }
      else
      {
        TankControl();
        delay(100);
      }
    }
    else
    {
      WaitForClick();        //Ожидание нажатия кнопки ENTER, перемещение по меню
      display.clearDisplay();
      main_menu.click();     //Вызов функции отрисовки следующего пункта меню
      delay(400);
    }
  }
}

void loop()
{
}

