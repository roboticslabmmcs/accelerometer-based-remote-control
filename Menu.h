#ifndef MENU
#define MENU

#include <Adafruit_SSD1306.h>
#define MAX_AVALIABLE_DEVICES    6

class Menu
{
  public:
    struct Node
    {
      String title;
      int cursor_shift;
      void(*action)();
    };

    Node list[MAX_AVALIABLE_DEVICES];
    int size;

    Menu();
    ~Menu();

    void print(Adafruit_SSD1306 &display);
    void goForward(Adafruit_SSD1306 &display);
    void click();
    int current_position;

    Node& operator[](int index);
};

#endif


