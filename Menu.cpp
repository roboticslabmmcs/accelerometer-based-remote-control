#include "Menu.h"

Menu::Menu()
{
  size = MAX_AVALIABLE_DEVICES;
  for (int i = 0; i < size; ++i)
  {
    list[i].title = "";
    list[i].action = NULL;
    list[i].cursor_shift = 0;
  }
	current_position = 0;
}

Menu::~Menu(){}

void Menu::goForward(Adafruit_SSD1306 &display)
{
	current_position++;
	if (current_position == size)    
		current_position = 0;
  this->print(display);             
}

void Menu::click()
{
  if (list[current_position].action != NULL)
    list[current_position].action();
}

void Menu::print(Adafruit_SSD1306 &display)
{
	display.clearDisplay();
	for (int i = 0; i < this->size; ++i)
	{
    if (i == current_position)
    {
      display.setCursor(0,i*10);
      display.println("=>");
    }
		display.setCursor(list[i].cursor_shift+15, i*10);
		display.println(list[i].title);
	}
	display.display();
}

Menu::Node& Menu::operator[](int index)
{
  return list[index];
}


